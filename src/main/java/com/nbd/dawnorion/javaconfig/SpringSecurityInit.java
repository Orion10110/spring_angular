package com.nbd.dawnorion.javaconfig;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Artur on 04.08.2017.
 */
public class SpringSecurityInit extends AbstractSecurityWebApplicationInitializer {
}
