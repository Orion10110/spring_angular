package com.nbd.dawnorion.mvc.quarts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

/**
 * Created by Artur on 01.08.2017.
 */
/**
 * look application-context.xml
 * 1. simpleTrigger
 * 2. simpleQuartzJob
 * 3. bean id="simpleQuartzTask"
 * 4. Quartz Scheduler
 */
public class QuartzTask {

    private static final Logger logger = LoggerFactory.getLogger(QuartzTask.class);

    public void simpleTaskMethod() {
//		you can log here to database with simpletrigger
        logger.info("Test Simple Quartz Time: " + Calendar.getInstance().getTime());
        System.out.println("Test Simple Quartz Time: " + Calendar.getInstance().getTime());
    }

}