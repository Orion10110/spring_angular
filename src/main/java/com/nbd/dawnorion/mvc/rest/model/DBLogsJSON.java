package com.nbd.dawnorion.mvc.rest.model;

import java.util.List;

/**
 * Created by Artur on 02.08.2017.
 */
public class DBLogsJSON {

    private List<DBLogJSON> logList;

    public List<DBLogJSON> getLogList() {
        return logList;
    }

    public void setLogList(List<DBLogJSON> logList) {
        this.logList = logList;
    }
}

