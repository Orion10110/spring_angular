package com.nbd.dawnorion.mvc.rest.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Artur on 02.08.2017.
 */
@XmlRootElement(name = "LOGS")
public class DBLogsXML {

    private List<DBLogXML> logList;

    @XmlElement(name = "LOG")
    public List<DBLogXML> getLogList() {
        return logList;
    }

    public void setLogList(List<DBLogXML> logList) {
        this.logList = logList;
    }
}